# Simple CI gateway

**WARNING**: The generated image *will* wipe one drive clean, so don't use that
on a machine you care about!

This project aims to create a simple base to run any container-based CI infrastructure which follow these design principles:

 * Stateless: The CI system should be rebootable at any point of the testing, and the only jobs that could be affected are the ones that were currently running on the infra.
 * Testable: The CI system should not be worked on in production, no matter how low-level it is. Every component has to be testable on its own.
 * Secure: The devices that run the tests should not have access to the outside world. Work should be submitted by a gateway with 2 network interfaces, one connected to the internet, and one to the devices under test.

What this project does is generate an x86_64 bootable image that:

 * Automatically labels network interfaces (public / private)
 * Connects to the public network using DHCP
 * Starts a openssh server, accessible only using a locally-generated ssh key
 * Partition the first suitable drive and expose it in /mnt/tmp and /mnt/permanent
 * Starts the wanted infra service

## Building the image

The role of this project is simply to abstract the hardware that will be running your gateway services. You thus need to provide the services that will run.

The command to generate the gateway's image depends on the type of infrastructure you will run:

 * Docker-based infrastructure: `make infra_docker CONTAINER=hello-world`
 * Remote script execution: `make infra_web URL=https://example.com/script.sh EXTRA_PACKAGES="git vim wget docker-compose"`
 * Local app: `make infra_local APP=/path/to/folder/with/app EXTRA_PACKAGES="git vim wget docker-compose"`
 * More will be coming soon...

If you decide to use the local app method, the path you set should be towards a folder containing all your application's logic, and an executable file called `entrypoint`.

The command should take less than 5 minutes to complete, and if everything went well, you should have the following files in the out/ folder:

    $ ls out/
    gateway.iso  gateway_key  gateway_key.pub

The first file, `gateway.iso`, is the bootable OS image. The other two files are the SSH keys needed to connected to the root user of the image.

NOTE: Add `FAST_COMPRESSION=1` if you are iterating on the infrastructure and would like to save time at the expense of compression level.

## Testing the image

To test the image, there is an easy way to do so by using qemu. The following command will set up everything for you, including the bridge network:

    $ make run

To log in, just use the login `root` (no password necessary).

If you receive permission errors from QEMU, you might be running on a
distribution that disables the setuid bit for the
qemu-bridge-helper. On Ubuntu for example, the following is required
to launch QEMU without sudo,

    sudo chmod u+s /usr/lib/qemu/qemu-bridge-helper

Additionally, on Ubuntu at least, you need the following configuration
for the bridge helper,

    echo 'allow bridge_priv' | sudo tee /etc/qemu/bridge.conf 

Additionally, while the virtual machine is running, you can run the self tests:

    $ make test
    Warning: Permanently added '[localhost]:5555' (ECDSA) to the list of known hosts.
    ..
    ----------------------------------------------------------------------
    Ran 2 tests in 0.001s

    OK

To connect to the virtual machine using SSH, simply type:

    $ make connect
    ...

Finally, if the infra service you decided to run includes a TFTP server, you may want to test it by running:

    $ make start_netboot_dut
    ...

That's all there is!


## Flashing the image on a drive

Once the image has been generated, you may flash it on a medium of your choice (USB, or HDD/SDD/NVME drive) by running the following command:

    sudo dd if=out/gateway.iso of=/dev/sdX bs=4M status=progress oflag=sync

Plug the drive to the machine that will be used as a gateway. Make sure it has 2 network interfaces: the public requires a DHCP server while the private one should not be able to access any DHCP server.

Upon booting, you will be greeted by a minimal dashboard showing the result of the self tests (executed every 2 seconds), and some usage statistics:

<div align="center">
    ![Dashboard that loads after boot](doc/dashboard.png?inline=false)
</div>

To connect to the gateway, use the following command:

    $ make connect HOST="IP on the public interface"

That's all :)

## To do

Here are a list of tasks that would make this project better:

 * Add a watchdog service to reboot the machine in case of an issue
