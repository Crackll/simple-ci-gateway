SHELL := /bin/bash

HOST ?= localhost
ifeq ($(HOST), localhost)
	PORT ?= 5555
else
	PORT ?= 22
endif

DOCKER ?= docker

PRIV_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))
PUBLIC_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))

.DEFAULT_GOAL := run

_build:
	@-rm -r out 2> /dev/null
	@-$(DOCKER) rm simple_ci_build > /dev/null 2> /dev/null
	$(DOCKER) run --privileged --name simple_ci_build -e METHOD=$(METHOD) $(PARAMS) -e EXTRA_PACKAGES="$(EXTRA_PACKAGES)" -e FAST_COMPRESSION=$(FAST_COMPRESSION) -v $(PWD):/app_ro/ registry.freedesktop.org/mupuf/simple-ci-gateway/base_image:latest /app_ro/docker_gen_image.sh
	$(DOCKER) cp simple_ci_build:/app/out out/ > /dev/null
	$(DOCKER) rm simple_ci_build > /dev/null

infra_docker:
	@if test "$(CONTAINER)" = "" ; then \
		echo "Usage: make infra_docker CONTAINER=registry.freedesktop.org/gfx-ci/radv-infra:latest"; \
		exit 1; \
	fi
	$(MAKE) _build METHOD=docker PARAMS="-e CONTAINER=$(CONTAINER)"

infra_web:
	@if test "$(URL)" = "" ; then \
		echo "Usage: make infra_web URL=https://example.com/script.sh"; \
		exit 1; \
	fi
	$(MAKE) _build METHOD=web PARAMS="-e URL=$(URL)"

infra_local:
	@if test "$(APP)" = "" ; then \
		echo "Usage: make use_local APP=/full/path/to/script/folder"; \
		exit 1; \
	fi
	@[[ "$(APP)" =~ ^/ ]] || (echo "Error: The path to your application should be absolute"; exit 1)
	@[ -x "$(APP)/entrypoint" ] || (echo "Error: the file '$(APP)/entrypoint' should be executable"; exit 1)
	$(MAKE) _build METHOD=local PARAMS="-e APP=$(APP) -v $(APP):/usr_app/"

run: out/gateway.iso
	[ -f out/disk.img ] || fallocate -l 5G out/disk.img
	@ip link show bridge_priv 2> /dev/null > /dev/null || ( \
		echo "Setting up the bridge. This requires root rights!"; \
		sudo ip link add bridge_priv type bridge; \
		sudo ip link set bridge_priv up; \
		sudo iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT; \
	)
	@grep -e "^allow bridge_priv$$" /etc/qemu/bridge.conf > /dev/null 2> /dev/null || (echo "ERROR: Please run the following command: # echo 'allow bridge_priv' | tee -a /etc/qemu/bridge.conf"; exit 1)
	qemu-system-x86_64 -m size=4096 -k en -device virtio-scsi-pci,id=scsi0 -device scsi-cd,bus=scsi0.0,drive=cdrom0 -drive id=cdrom0,if=none,format=raw,media=cdrom,readonly=on,file=out/gateway.iso -display gtk -vga std -device virtio-net-pci,romfile=,netdev=net0,mac=$(PUBLIC_MAC) -netdev user,id=net0,hostfwd=tcp::5555-:22 -net nic,macaddr=$(PRIV_MAC),model=virtio-net-pci -net bridge,br=bridge_priv -drive file=out/disk.img,format=raw,index=0,media=disk -enable-kvm -serial stdio -boot d -no-reboot

start_netboot_dut:
	qemu-system-x86_64 -m size=1024 -net nic,macaddr=$(PRIV_MAC),model=virtio-net-pci -net bridge,br=bridge_priv -boot n

test:
	@ssh -i out/gateway_key root@localhost -p 5555 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null /usr/bin/selftest

connect:
	@ssh -i out/gateway_key root@${HOST} -p ${PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

clean:
	-rm out/gateway.iso out/gateway_key.pub out/gateway_key
	-$(DOCKER) rm simple_ci_build
